@yield('content')

@extends('layouts.app')
@section('content')

<h1> This is the customers list</h1>

  

 <a href = "{{route('customers.create')}}"> Create a new customer</a>
<table>
<tr>
    <th>Customer name</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Edit</th>
    <th>Delete</th>
    @cannot('salesrep') <th>Delete</th> @endcannot
    
    
</tr>
    @foreach($customers as $customer)
<tr>
    <td> {{$customer->name}}</td>
    <td> {{$customer->email}}</dt>
    <td> {{$customer->phone}}</dt>
    
    <td> <a href = "{{route('customers.edit',$customer->id)}}"> Edit</a> </td>
   
    <td> 

    <form method = 'post' action = "{{action('ExamController@destroy', $customer->id)}}" > 
  
    @csrf
     @method('DELETE')

    <div class = "form-group">
        <input type = "submit" class = "form-control" name = "submit" value = "Delete">
    </div>
    </td> 
    </form>
    
</tr>
    @endforeach
    </table>

   

    

    @endsection

    <style>
table, th, td {
  border: 1px solid black;
}
    </style>

