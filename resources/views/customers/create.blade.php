@extends('layouts.app')

@section('content')
<h1>Create a new customer!</h1>
<form method="post" action="{{action('ExamController@store')}}">
@csrf
          <div class="form-group">    
              <label for="name">What is the customer name?</label>
              <input type="text" class="form-control" name="name"/>
          </div>
        <div class="form-group">    
              <label for="email">What is the customer email?</label>
              <input type="text" class="form-control" name="email"/>
          </div>
          <div class="form-group">    
              <label for="phone">What is the customer phone?</label>
              <input type="text" class="form-control" name="phone"/>
          </div>
          <div class="form-group">    
              <input type="submit" class="btn btn-primary" name="submit" value = "save customer"/>
          </div>  
      
</form> 
@endsection