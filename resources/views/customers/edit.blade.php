@extends('layouts.app')

@section('content')
<form method="post"  action="{{action('ExamController@update', $customer->id) }}">
 @method('PATCH')
 @csrf
 <div class="form-group">
   <label for="item">Update customer name</label>
   <input type="text" class="form-control" name="name" value="{{ $customer->name}}" />
 </div>
 <div class="form-group">
   <label for="item">Update customer email</label>
   <input type="text" class="form-control" name="email" value="{{ $customer->email}}" />
 </div>
 <div class="form-group">
   <label for="item">Update customer phone</label>
   <input type="text" class="form-control" name="phone" value="{{ $customer->phone}}" />
 </div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection