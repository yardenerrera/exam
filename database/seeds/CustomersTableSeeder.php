<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert
        ([
            [    'name' => 'Yarden',
                'email' => 'yarden@y.com',
                'phone' => '0505566565',
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 1
            ],
            [     
            'name' => 'Jonny',
            'email' => 'Jonny@j.com',
            'phone' => '0505456565',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 2
        ],
  
    ]);

    }
}
