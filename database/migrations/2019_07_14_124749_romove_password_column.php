<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RomovePasswordColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

        public function up()
        {
            Schema::table('customers', function($table) {
               $table->dropColumn('password');
            });
        }
  
        public function down()
        {
            Schema::table('customers', function($table) {
               $table->string('password');
               
            });
        }


    }